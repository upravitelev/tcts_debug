---
title: "Дебаггинг в R без инсектицидов"
author: "Philipp A.Upravitelev <br> <upravitelev@gmail.com> <br>"
date: '`r Sys.Date()`'
output:
  ioslides_presentation: 
    autosize: yes
    logo: ./pics/tcts_logo.png
    transition: slower
    widescreen: yes
---

```{r, include=FALSE}
knitr::opts_chunk$set(echo = FALSE,
                      message = FALSE,
                      cache = TRUE)

library(data.table)
setDTthreads(4)

library(dplyr)
library(microbenchmark)
library(futile.logger)
library(profvis)

source('./src/utils.R')
options(digits = 3)
```


# about me 

##

 product analyst at Pixonic
 
 product analyst at GameInsight
 
 analyst at ConsultantPlus (Консультант+)
 
 PhD-student at SPbSU (cognitive psychology)

# Performance

## tools

system.time()

tictoc package

bench package

microbencmark package

## microbenchmark {.build}

```{r, eval = FALSE}
library(microbenchmark)

a <- 2
f <- function(x, y) x ^ y

microbenchmark(
  '2 + 2' = 2 ^ 2, 
  '2 + a' = 2 ^ a, 
  'f(2, 2)' = f(2, 2), 
  'f(2, a)' = f(2, a),
  times = 100
)
```

----

```{r, echo = FALSE}
library(microbenchmark)

a <- 2
f <- function(x, y) x ^ y

microbenchmark(
  '2 ^ 2' = 2 ^ 2, 
  '2 ^ a' = 2 ^ a, 
  'f(2, 2)' = f(2, 2), 
  'f(2, a)' = f(2, a),
  times = 1000
)
```

## Задание

Напишите оценку скорости агрегации по `v1` и расчета среднего (или суммы) значения `v2` таблицы `my_frame`. Сравните base R, data.table и/или dplyr

```{r}
N <- 1e5
set.seed(1234)
my_frame <- data.frame(
  v1 = sample(sample(letters, 3), N, replace = TRUE),
  v2 = rnorm(N)
)

my_frame_dt <- as.data.table(my_frame)
my_frame_tb <- as_tibble(my_frame)
```

----

```{r}
f1 <- function() aggregate(v2 ~ v1, my_frame, sum)
f2 <- function() aggregate(my_frame$v2, by = list(my_frame$v1), sum)

microbenchmark(f1(), f2(), times = 100)
```


## Решение
```{r}
fn <- mean
f1 <- function() aggregate(v2 ~ v1, my_frame, fn)
f2 <- function() my_frame_dt[, list(v2 = fn(v2)), keyby = v1]
f3 <- function() my_frame_tb %>% group_by(v1) %>% summarise(v2 = fn(v2))
```

----

```{r}
microbenchmark(
  'base R' = f1(),
  'data.table' = f2(),
  'dplyr' = f3(),
  times = 100
)
```


# Profiling

## Задача из заявки

<!-- тут нужен текст заявки -->

----

```{r}
numbers_1 <- function(x) {
  x <- as.character(x)
  x <- strsplit(x, "")
  x <- unlist(x)
  digit.vector <- factor(x, levels = 0:9)
  res <- table(digit.vector)
  as.numeric(res)
}
```

----

```{r}
numbers_2 <- function(x) {
  x <- paste(x, collapse = "")
  z <- nchar(x)
  x <- as.double(x)
  y <- rep(0, 10)
  for (i in seq_len(z)) {
    k <- x %% 10
    y[k + 1] <- y[k + 1] + 1
    x <- x %/% 10
  }
  y
}
```

----

```{r}  
x <- c(101, 58, 96, 281)
identical(
  numbers_1(x),
  numbers_2(x)
)
```

## benchmark
```{r}
vec <- c(101, 58, 96, 281)

microbenchmark(
  'numbers_1' = numbers_1(vec),
  'numbers_2' = numbers_2(vec),
  times = 100
)
```

<!-- увеличиваем массив -->

```{r, eval=FALSE, echo=FALSE}
vec <- sample(1000, 1e5, replace = TRUE)

microbenchmark(
  'numbers_1' = numbers_1(vec),
  'numbers_2' = numbers_2(vec),
  times = 10
)
```


## profvis {.build}

```{r, eval=FALSE}
library(profvis)

profvis(numbers_1(vec))
```

```{r, eval = FALSE}
profvis(numbers_2(vec))
```

```{r}
vec <- sample(1000, 1e5, replace = TRUE)
```

----

```{r}
profvis({
  numbers_1(vec)
  numbers_2(vec)
})
```

# Optimization

## vectorization {.build}

```{r}
vec <- rnorm(5)
vec
```

```{r}
round(vec, 3)
```

## loops {.build}

```{r}
N <- 1e4
set.seed(12345)

uids <- paste0('uid_', sample(N), sample(letters, N, TRUE))
uids[1:3]
```

```{r}
uids_splitted <- strsplit(uids, '_')
uids_splitted[[1]][2]
```

##  {.build}

```{r}
uids_second <- c()
for (i in seq_len(N))
  uids_second <- c(uids_second, uids_splitted[[i]][2])
uids_second[1:3]
```

## Задание

Сделайте профилирование цикла
```{r, eval=FALSE}
uids_second <- c()
for (i in seq_len(N))
  uids_second <- c(uids_second, uids_splitted[[i]][2])
```

```{r, echo = FALSE, eval = FALSE}
uids_second <- vector('character', N)
for (i in seq_len(N))
  uids_second[i] <- uids_splitted[[i]][2]
uids_second[1:3]
```

## Решение
```{r}
loop1 <- function(x) {
  uids_second <- c()
  for (i in seq_len(N))
    uids_second <- c(uids_second, x[[i]][2])
  uids_second
}
```

```{r}
profvis(loop1(uids_splitted))
```

## Преаллокация памяти {.build}

```{r}
loop2 <- function(x) {
  uids_second <- vector('character', N)
  for (i in seq_len(N))
    uids_second[i] <- x[[i]][2]
  uids_second
}
```

## {.build}

```{r, eval=FALSE}
microbenchmark(
  'c()' = loop1(uids_splitted), 
  'vector()' = loop2(uids_splitted),
  times = 100
)
```

```{r, echo=FALSE}
microbenchmark(
  'c()' = loop1(uids_splitted), 
  'vector()' = loop2(uids_splitted),
  times = 100
)
```


# Debugging

## Steps {.build}

Если вы баг не видите, это не значит, что его нет

Найдите, где ошибка

Убедитесь в повторяемости ошибки

Исправьте и протестируйте

## traceback() {.build}
```{r, eval=FALSE}
fun_h(5)
```

```{r, echo=FALSE, cache=TRUE}
try(fun_h(5))
```

```{r, eval=FALSE}
traceback()
```

## print()

Дебажу принтом!

## debug()
```{r, eval=FALSE}
fun_d <- function(mu, val){
  sub1 <- val - mu
  sqr <- sub1 ^ 2
  gt <- sum(sqr)
  gt
}
```

```{r}
set.seed(12345)
val <- rnorm(100)
fun_d(1, val)
```


## debug tools

- debug() / undebug()

- debugonce()

- breackpoints

## debugonce()

```{r, eval=FALSE}
debugonce(fun_d)
fun_d(1, val)
```

## Задание

Найдите ошибки в функции `numbers_err()`

# Defensive programming

## Principles

 Fail fast: чем быстрее упадет, тем лучше
 
 Fail safe: когда упадет, не должно критично повлиять на данные
 
 Fail conspicuously: сообщение об ошибке должно быть понятным
 
 Fail appropriately: ошибки должны быть обработаны
 
 Fail creatively: не все, что падает - плохо

##

Спасение утопающих - дело рук самих утопающих! 
Стелите соломку.

## stopifnot(), is() etc

```{r}
fun_t <- function(x) {
  if (x == TRUE) {
    print('Все хорошо, прекрасная маркиза')
  } else {
      print('Все пропало, шеф!')
  }
}
```

##

```{r}
fun_t <- function(x) {
  stopifnot(is.logical(x))
  
  if (x == TRUE) {
    print('Все хорошо, прекрасная маркиза')
  } else {
    print('Все пропало, шеф!')
  }
}
```

## Задание

Импортируйте все файлы в папке `./data/exp`. Используйте for/*pply + `list.files()`

# Logs

## messages

message('all is fine')

warning('hmmmm you should know')

stop('grob grob smert kladbishche')


## futile.logger
```{r, eval=FALSE}
library(futile.logger)

log_format <- layout.format('~t ~f [~l]: ~m')
flog.layout(log_format)
```

```{r, eval=FALSE}
log_file <- file.path('.', 'logs', paste(Sys.Date(), 'log', sep = '_'))
flog.appender(appender.file(log_file))
```

##

```{r, eval=FALSE}
flog.info('just info')
```

## sessionInfo()

```{r}
sessionInfo()
```

## sessionInfo() + flog.info()
```{r, eval=FALSE}
session_info <- paste(c("SESSION INFO", capture.output(sessionInfo())), collapse = "\n")

flog.info(session_info)
```

# good practices

## {.build}

Пишите документацию.

In God we trust, everyone else we verify.

Функции должны быть короткими

Явный вызов внешних библиотек

Не используйте require() и setwd()

Строго контролируйте зависимости пакетов

Style-guide и инструменты для форматирования кода

По возможности, пишите пакеты

-------

<div style="position:absolute;bottom:10%;right:6%;">
![](./pics/tzeentch2.png)
</div>



